//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/11/2014.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <iostream>
#include "Oscillator.h"

class SquareOscillator : public Oscillator
{
public:
	//==============================================================================
	/**
	 SquareOscillator constructor
	 */
	SquareOscillator();
	
	/**
	 SquareOscillator destructor
	 */
	~SquareOscillator();
        
    /**
	 function that provides the execution of the waveshape
	 */
	float renderWaveShape (const float currentPhase) override;
	

};

#endif //H_SQUAREOSCILLATOR
