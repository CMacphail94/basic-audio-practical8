//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/11/2014.
//
//

#include "SquareOscillator.h"


SquareOscillator::SquareOscillator()
{
    reset();
}

SquareOscillator::~SquareOscillator()
{
    
}

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    float res;
    if(currentPhase > (M_PI))
        res = -1;
    else
        res = 1;
    
    return res;

    
}