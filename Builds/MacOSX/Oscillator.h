//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/11/2014.
//
//

#ifndef H_OSCILLATOR
#define H_OSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Abstract class for oscillator
 */

class Oscillator
{
public:
	//==============================================================================
	/**
	 Oscillator constructor
	 */
	Oscillator();
	
	/**
	 Oscillator destructor
	 */
	virtual ~Oscillator();
	
	/**
	 sets the frequency of the oscillator
	 */
	void setFrequency (float freq);
	
	/**
	 sets frequency using a midi note number
	 */
	void setNote (int noteNum);
	
	/**
	 sets the amplitude of the oscillator
	 */
	void setAmplitude (float amp);
	
	/**
	 resets the oscillator
	 */
	void reset();
	
	/**
	 sets the sample rate
	 */
	void setSampleRate (float sr);
	
	/**
	 Returns the next sample
	 */
	float nextSample();
	
	/**
	 function that provides the execution of the waveshape
	 */
	virtual float renderWaveShape (const float currentPhase) = 0;
	
protected:
	float frequency;
    float amplitude;
    float phase;
private:
    float sampleRate;
    float phaseInc;
};

#endif //H_OSCILLATOR